package com.zj.zhao.dependency;

import com.zj.zhao.dependency.bean.A;
import com.zj.zhao.dependency.bean.B;
import com.zj.zhao.dependency.processor.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BeanFactoryPostProcessorDemo {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(A.class);
        context.register(B.class);

        context.register(MyPriorityOrderedBeanDefinitionRegistryPostProcessor.class);
        context.register(MyOrderedBeanDefinitionRegistryPostProcessor.class);
//        context.addBeanFactoryPostProcessor(new MyPriorityOrderedBeanDefinitionRegistryPostProcessor());
//        context.addBeanFactoryPostProcessor(new MyOrderedBeanDefinitionRegistryPostProcessor());

//        context.register(PriorityOrderedBeanFactoryPostProcessor.class);
//        context.register(OrderedBeanFactoryPostProcessor.class);
//        context.addBeanFactoryPostProcessor(new PriorityOrderedBeanFactoryPostProcessor());
//        context.addBeanFactoryPostProcessor(new OrderedBeanFactoryPostProcessor());
//        context.addBeanFactoryPostProcessor(beanFactory -> {
//            beanFactory.addBeanPostProcessor(new BeanPostProcessor() {
//
//                @Override
//                public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//                    if (beanName.equals("a")) {
//                        System.out.printf("%s Bean 名称:%s 在初始化后回调...%n", bean.getClass().getName(), beanName);
//                    }
//                    return bean;
//                }
//            });
//        });

        context.refresh();

        // 显示关闭
        context.close();
    }
}
