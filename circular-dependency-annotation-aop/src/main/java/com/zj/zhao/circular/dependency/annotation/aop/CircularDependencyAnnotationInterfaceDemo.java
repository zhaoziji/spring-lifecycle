package com.zj.zhao.circular.dependency.annotation.aop;

import com.zj.zhao.circular.dependency.annotation.aop.bean.A;
import com.zj.zhao.circular.dependency.annotation.aop.bean.B;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("com.zj.zhao.circular.dependency.annotation.aop")
@EnableAspectJAutoProxy
public class CircularDependencyAnnotationInterfaceDemo {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(CircularDependencyAnnotationInterfaceDemo.class);

        context.refresh();

        A a = context.getBean(A.class);
        B b = context.getBean(B.class);

        System.out.println("a = " + a);
        System.out.println("a.b = " + a.getB());
        System.out.println("b.a = " + b.getA());

        // 显示关闭
        context.close();
    }
}
