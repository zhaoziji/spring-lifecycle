package com.zj.zhao.circular.dependency.annotation.aop.bean;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MyAspect {
    // 前置通知
    @Before("execution( * com.zj.zhao.circular.dependency.annotation.aop.bean.A.getB(..) )")
    public void beforeRun() {
        System.out.println("Before run.......");
    }
}
