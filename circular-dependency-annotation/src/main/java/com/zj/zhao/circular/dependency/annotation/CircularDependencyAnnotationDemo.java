package com.zj.zhao.circular.dependency.annotation;

import com.zj.zhao.circular.dependency.xml.bean.A;
import com.zj.zhao.circular.dependency.xml.bean.B;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CircularDependencyAnnotationDemo {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(A.class);
        context.register(B.class);

        context.refresh();

        A a = context.getBean(A.class);
        B b = context.getBean(B.class);

        System.out.println("a.b = " + a.getB());
        System.out.println("b.a = " + b.getA());

        // 显示关闭
        context.close();
    }
}
