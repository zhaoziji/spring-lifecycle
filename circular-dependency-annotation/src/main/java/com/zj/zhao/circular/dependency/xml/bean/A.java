package com.zj.zhao.circular.dependency.xml.bean;

import org.springframework.beans.factory.annotation.Autowired;

public class A {
    @Autowired
    private B b;

    public B getB() {
        return b;
    }

    // AnnotationConfigApplicationContext 是通过 Field#set(obj, value) 方法完成依赖属性的注入
//    public void setB(B b) {
//        this.b = b;
//    }
}
