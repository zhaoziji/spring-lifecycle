package com.zj.zhao.circular.dependency.xml;

import com.zj.zhao.circular.dependency.xml.bean.A;
import com.zj.zhao.circular.dependency.xml.bean.B;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CircularDependencyXmlDemo {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("META-INF/circular-context.xml");

        A a = context.getBean(A.class);
        B b = context.getBean(B.class);

        System.out.println("a.b = " + a.getB());
        System.out.println("b.a = " + b.getA());

        // 显示关闭
        context.close();
    }
}
