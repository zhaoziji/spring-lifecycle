package com.zj.zhao.circular.dependency.xml.bean;

public class A {
    private B b;

    public B getB() {
        return b;
    }

    // ClassPathXmlApplicationContext 需要通过 setter 方法完成依赖属性的注入: Method#invoke(obj, args)
    public void setB(B b) {
        this.b = b;
    }
}
