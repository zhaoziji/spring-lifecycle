package com.zj.zhao.reflection.annotation;

import java.lang.annotation.*;

// @Retention and @Target 是必须要的
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
// @Inherited and @Documented 是可选项
@Inherited
@Documented
public @interface MyAutowired {
}
