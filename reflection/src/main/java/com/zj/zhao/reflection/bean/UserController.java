package com.zj.zhao.reflection.bean;

import com.zj.zhao.reflection.annotation.MyAutowired;

public class UserController {

    private UserService userService;

    @MyAutowired
    private UserService anotherUserService;

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String toString() {
        return "UserController{" +
                "userService=" + userService +
                ", anotherUserService=" + anotherUserService +
                '}';
    }
}
